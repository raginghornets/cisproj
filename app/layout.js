function element(type, props = {}, children = []) {
  let element = document.createElement(type);
  for (key in props) {
    element[key] = props[key];
  }
  for (let i = 0; i < children.length; i++) {
    element.appendChild(children[i]);
  }
  return element;
}

function div(props = {}, children = []) {
  return element("div", props, children);
}

function input(props = {}, children = []) {
  return element("input", props, children);
}

function button(props = {}, children = []) {
  return element("button", props, children);
}

function table(props = {}, children = []) {
  return element("table", props, children);
}

function tr(props = {}, children = []) {
  return element("tr", props, children);
}

function th(props = {}, children = []) {
  return element("th", props, children);
}

function td(props = {}, children = []) {
  return element("td", props, children);
}

function label(props = {}, children = []) {
  return element("label", props, children);
}

function form(props = {}, children = []) {
  return element("form", props, children);
}

function select(props = {}, children = []) {
  return element("select", props, children);
}

function option(props = {}, children = []) {
  return element("option", props, children);
}

function getId(id) {
  return document.getElementById(id);
}