const editDatabase = module.exports;

const db = require("./db");
const render = require("./render");

const directory = "database";

editDatabase.createTable = function (req, res) {
  let queryString = 
  `CREATE TABLE IF NOT EXISTS ${req.body.tableName} (
      PRIMARY KEY (${req.body.primaryKey}),
      ${req.body.primaryKey} int NOT NULL,
      ${db.queryColumns(req, 2)}
  );`;
  let query = db.query(queryString, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/`);  
    }
  });
}

editDatabase.dropTable = function (req, res) {
  let queryString = `DROP TABLE IF EXISTS ${req.params.tableName};`;
  let query = db.query(queryString, (error, result) => {
    if (error) {
      render.renderError(res, error);
    }
    if (result) {
      res.redirect(`/${directory}/`);  
    }
  });
}