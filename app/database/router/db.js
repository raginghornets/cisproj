const mysql = require("mysql");

const db = mysql.createConnection({
  "host": process.env.host || "localhost",
  "user": process.env.user || "root",
  "database": process.env.database || "cisproj",
  "password": process.env.password || "password",
});

db.connect((error) => {
  if (error) {
    console.error("Failed to connect to MySQL.", error);
  } else {
    console.log("MySQL connected.");
  }
});

db.queryColumns = function (req, start = 0) {
  let query = "";
  let keys = Object.keys(req.body);
  for (let i = 0; i < keys.length; i++) {
    switch (keys[i]) {
      case "columnName":
        if (req.body["columnName"].constructor === Array) {
          for (let j = 0; j < req.body["columnName"].length; j++) {
            query += `${req.body["columnName"][j]} ${req.body["columnType"][j]}, `;
          }
        } else {
          query += `${req.body["columnName"]} ${req.body["columnType"]}`;
        }
        break;
      case "foreignKey":
        if (req.body["foreignKey"].constructor === Array) {
          for (let j = 0; j < req.body["foreignKey"].length; j++) {
            query += `
              CONSTRAINT FK_${req.body["foreignKey"][j]}
              FOREIGN KEY (${req.body["foreignKey"][j]})
              REFERENCES ${req.body["referenceTable"][j]}(${req.body["referenceId"][j]})`;
          }
        } else {
          query += `
            CONSTRAINT FK_${req.body["foreignKey"]}
            FOREIGN KEY (${req.body["foreignKey"]})
            REFERENCES ${req.body["referenceTable"]}(${req.body["referenceId"]})`;
        }
        break;
      default:
        break;
    }
  }

  // Remove trailing comma
  query = query.substr(query.length - 2, 2) === ", " ? query.substr(0, query.length - 2) : query;

  return query;
};

module.exports = db;
