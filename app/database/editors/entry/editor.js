const editor = document.getElementById("editor");

function labelColumn(columnName) {
  return td({
    "className": "p-2"
  }, [
    label({
      "for": columnName,
      "innerHTML": columnName
    })
  ]);
}

function editColumn(columnName, columnType, columnValue) {
  return td({
    "className": "p-2"
  }, [
    input({
      "className": "p-2 wp-1",
      "type": columnType,
      "name": columnName,
      "placeholder": columnName,
      "value": columnValue
    })
  ])
}

function addEditColumn(columnName, columnType, columnValue) {
  return tr({}, [
    labelColumn(columnName),
    editColumn(columnName, columnType, columnValue)
  ]);
}

function addReadColumn(columnName, columnValue) {
  return tr({ "onclick": () => alert("You can't edit this field.") }, [
    labelColumn(columnName),
    td({ "innerHTML": columnValue })
  ]);
}

function buttonSubmit() {
  return tr({}, [
    td({
      "className": "p-2",
      "colSpan": 2
    }, [
      button({
        "className": "p-2 wp-1",
        "type": "submit",
        "textContent": "Update"
      })
    ])
  ]);
}

function addColumns(entry) {
  let entryList = Object.keys(entry);
  for (let i = 0; i < entryList.length; i++) {
    if (i === 0) {
      editor.appendChild(addReadColumn(entryList[i], entry[entryList[i]]));
    } else {
      editor.appendChild(addEditColumn(entryList[i], "text", entry[entryList[i]]));
    }
  }
  editor.appendChild(buttonSubmit());
}